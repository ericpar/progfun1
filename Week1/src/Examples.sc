def abs(x: Double) = if (x < 0) -x else x

def sqrt(x:Double) = {

  def isGoodEnough(guess: Double, x: Double) =
    (abs(guess * guess - x) / x) < 0.001

  def improve(guess: Double, x: Double) =
    (guess + x / guess) / 2

  // Recursive function here !!!
  def sqrtIter(guess: Double, x: Double): Double =
    if (isGoodEnough(guess, x)) guess
    else sqrtIter(improve(guess, x), x)

  sqrtIter(1.0, x)
}

sqrt(2)
sqrt(4)
sqrt(1e-6)
sqrt(1e60)


// Remember ! Put the return type as this
// is a recursive function...
def gcd(a: Int, b: Int): Int =
  if (b == 0) a else gcd(b, a % b)

gcd(123, 321)
gcd(432, 532)