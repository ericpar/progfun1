//package week4

trait Expr
case class Number(n: Int) extends Expr
case class Sum(e1: Expr, e2: Expr) extends Expr


def eval(e: Expr): Int = e match {
  case Number(n) => n
  case Sum(e1, e2) => eval(e1) + eval(e2)
}
eval(Sum(Number(2), Number(3)))


def show(e: Expr): String = e match {
  case Number(x) => x.toString
  case Sum(lhs, rhs) => show(lhs) + " + " + show(rhs)
}
show(Sum(Number(3), Number(41)))
