#+TITLE:       Functional programming with Scala (week 1)
#+AUTHOR:      Eric Parent
#+EMAIL:       eric@eparent.info
#+OPTIONS:     toc:t
#+TOC:         headlines 2
#+STARTUP:     showall



* Recursion

** Type of recursive functions

Recursive functions need to be typed explicitly to avoid the compiler
to loop indefinetly while trying to determine the return type (... recursively).

Example


#+BEGIN_SRC

def abs(x: Double) = if (x < 0) -x else x

def isGoodEnough(guess: Double, x: Double) =
  (abs(guess * guess - x) / x) < 0.001

def improve(guess: Double, x: Double) =
  (guess + x / guess) / 2

// Recursive function here !!!
def sqrtIter(guess: Double, x: Double): Double =
  if (isGoodEnough(guess, x)) guess
  else sqrtIter(improve(guess, x), x)

#+END_SRC
