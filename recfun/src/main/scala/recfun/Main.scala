package recfun


object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      // The leftmostmost and the rightmostmost columns contain only value 1.
      if (c == 0 || c == r) 1
      /* The Pascal number somewhere in the triangle is the sum of
       the two neighbors in the previous line (or row). */
      else pascal(c - 1, r - 1) + pascal(c, r - 1)
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def balanceRecursive(chars: List[Char], nbParens: Int): Boolean = {
        // Balanced means having as much '(' than ')'
        if (chars.isEmpty) nbParens == 0
        // ')' can never occure before '('
        else if (nbParens < 0) false
        // Not a parenthesis ?
        else if (chars.head != '(' && chars.head != ')') balanceRecursive(chars.tail, nbParens)
        // Opening parenthesis ?
        else if (chars.head == '(') balanceRecursive(chars.tail, nbParens + 1)
        // Closing parenthesis ?
        else if (chars.head == ')') balanceRecursive(chars.tail, nbParens - 1)
        // Otherwise, it must be false
        else false
      }
      balanceRecursive(chars, 0)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {

      def changeIter(money: Int, coins: List[Int]): Int = {

        if (money == 0) 1
        else if (money < 0) 0
        else if ((money > 0) && coins.isEmpty) 0 // there is still money to return but no more pennies
        else changeIter(money, coins.tail) + changeIter(money - coins.head, coins)
      }
      changeIter(money, coins)
    }
  }
