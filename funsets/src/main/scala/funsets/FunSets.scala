package funsets


/**
 * 2. Purely Functional Sets.
 */
object FunSets {

  /**
   * We represent a set by its characteristic function, i.e.
   * its `contains` predicate.
   */
  type Set = Int => Boolean

  /**
   * Indicates whether a set contains a given element.
   */
  def contains(s: Set, elem: Int): Boolean = s(elem)

  /**
   * Returns the set of the one given element.
   */
    def singletonSet(elem: Int): Set = (n: Int) => n == elem


  /**
   * Returns the union of the two given sets,
   * the sets of all elements that are in either `s` or `t`.
   *
   * We use anonymous function to represent the operation.
   *
   */
    def union(s: Set, t: Set): Set = (n: Int) => s(n) || t(n)

  /**
   * Returns the intersection of the two given sets,
   * the set of all elements that are both in `s` and `t`.
   *
   * We use anonymous function to represent the operation.
   * Element has to be found in both sets (&&)
   */
    def intersect(s: Set, t: Set): Set = (n: Int) => s(n) && t(n)

  /**
   * Returns the difference of the two given sets,
   * the set of all elements of `s` that are not in `t`.
   *
   * We use anonymous function to represent the operation.
   * Element has to be found in the left-hand set but not in the right-hand set.
   */
    def diff(s: Set, t: Set): Set = (n: Int) => s(n) && !t(n)

  /**
   * Returns the subset of `s` for which `p` holds.
   *
   * The given element must be both in the set and also satisfy the predicate passed as argument.
   */
    def filter(s: Set, p: Int => Boolean): Set = (element: Int) => s(element) && p(element)


  /**
   * The bounds for `forall` and `exists` are +/- 1000.
   */
  val bound = 1000

  /**
   * Returns whether all bounded integers within `s` satisfy `p`.
   */
    def forall(s: Set, p: Int => Boolean): Boolean = {
    def iter(a: Int): Boolean = {
      if (a > bound) true  // we must set this to 'true' otherwise we will always get 'false' due to the final recursive call
      else if (contains(s, a)) p(a) &&  iter(a + 1)
      else iter(a + 1)
    }
    iter(0)
  }

  /**
   * Returns whether there exists a bounded integer within `s`
   * that satisfies `p`.
   */
    def exists(s: Set, p: Int => Boolean): Boolean = !forall(s, x => !p(x))

  /**
   * Returns a set transformed by applying `f` to each element of `s`.
   */
    def map(s: Set, f: Int => Int): Set = (y: Int) => exists(s, x => f(x) == y)

  /**
   * Displays the contents of a set
   */
  def toString(s: Set): String = {
    val xs = for (i <- -bound to bound if contains(s, i)) yield i
    xs.mkString("{", ",", "}")
  }

  /**
   * Prints the contents of a set on the console.
   */
  def printSet(s: Set) {
    println(toString(s))
  }
}
